# MGLApps

## Mobile GNU/Linux Apps

This project is served at [https://mglapps.frama.io/](https://mglapps.frama.io/).

Mobile GNU/Linux Apps (short: MGLApps) is a list of (potential) applications for usage on mobile devices running GNU/Linux, having small(er) screens and touch input (e.g. smartphones, tablets, convertibles).

Files:
* [index.html](index.html): Main page hosted at [https://mglapps.frama.io/](https://mglapps.frama.io/)
* [apps.csv](apps.csv): Main app list
* [other apps.csv](other apps.csv): Further apps which are not added to the main list yet, because they are not in a usable state, are still in planning stage or wait to be transfered to the main list

MGLApps is licensed under CC BY-SA 4.0 International: [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/) . For more licensing information, see [LICENSE](LICENSE)

For more information regarding this project, please see [https://mglapps.frama.io/](https://mglapps.frama.io/)
